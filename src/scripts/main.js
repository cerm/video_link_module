/**
 * Created by Anders on 03/02/16.
 */

var debugCounter = 0;
var threshold = 100;

var debug = false;
var caching = true;

var interface;

function setup() {

    $.ajaxSetup({
        cache: caching
    });

    // var gitHack = 'https://bb.githack.com/anhesr/api/raw/1.1.1/AbstractInterface.js';

    // $.getScript('scripts/AbstractInterface.js', function () {
        console.log('interface loaded');
        interface = new AbstractInterface();
        configurePlayer();
        console.log(interface);
    // });
}

function setupButton(buttonName) {
    var showButton = false;

    try {
        if (typeof interface.getVariable('buttonText') !== 'undefined' && interface.getVariable('buttonText') !== '') {
            showButton = true;
            if (typeof interface.getVariable('buttonStyle') !== 'undefined') {
                var styleArray = interface.getVariable('buttonStyle').split(';');
                var bgColor = '';
                for (var i = 0; i < styleArray.length; i++) {
                    if (styleArray[i].indexOf('background') != -1) {
                        bgColor = styleArray[i].split(':')[1];
                    }
                }
                $('#' + buttonName).attr('style', interface.getVariable('buttonStyle'));
                if (typeof interface.getVariable('buttonHoverColor') !== 'undefined') {
                    // var hoverColor = interface.getVariable('buttonHoverColor');
                    $('#' + buttonName).hover(function () {
                        $(this).css("background", interface.getVariable('buttonHoverColor'));
                    }, function () {
                        $(this).css("background", bgColor);
                    });
                }
            }
        }
    } catch (e) {}
    return showButton;
}

function setupInternalNavigation(myApp) {
    var cpList = interface.getVariable('cpList');
    var jumpFromList = interface.getVariable('jumpFromList');
    var jumpToList = interface.getVariable('jumpToList');
    if (cpList || (jumpFromList && jumpToList)) {
        if(cpList){
            var cpArray = (typeof cpList !== 'undefined' && cpList != '') ? cpList.split(',') : null;
        }
        if(jumpFromList && jumpToList){
            var jumpFromArray = (typeof jumpFromList !== 'undefined'  && jumpFromList != '') ? jumpFromList.split(',') : null;
            var jumpToArray = (typeof jumpToList !== 'undefined' && jumpToList != '') ? jumpToList.split(',') : null;
        }
        myApp.bind("player:video:timeupdate", function (eventName, obj) {
            myApp.get("currentTime", function (value) {
                if (debug) {
                    console.log("Value of 'currentTime' is: " + value);
                }
                _.forEach(cpArray, function (cpVal, cpKey) {
                    if (_.round(value) == cpVal) {
                        console.log('hit for cpVal: ' + cpVal + ", cpKey: " + cpKey);
                        interface.setVariable('cpHit' + cpKey, 1);
                    }
                });
                _.forEach(jumpFromArray, function (jumpVal, jumpKey) {
                    if (_.round(value) == jumpVal) {
                        console.log('hit for jumpVal: ' + jumpVal + ", jumpKey: " + jumpKey);
                        console.log('jumpTo: ' + jumpToArray[jumpKey]);
                        myApp.set('currentTime', Number(jumpToArray[jumpKey]));
                        // myApp.controller.currentTime = jumpToArray[jumpKey];
                    }
                });
            });

        });
    }
}

function navigate() {
    if (typeof interface.getVariable('scrubInMS') !== 'undefined' && interface.getVariable('scrubInMS') != '') {
        interface.scrubInMS(Number(interface.getVariable('scrubInMS')));
        interface.setVariable('scrubInMS', '');
    } else {
        interface.jumpToSlide(interface.getVariable('jumpToSlideIndex'));
    }
}

function configurePlayer() {
    var source = interface.getVariable('videoUrl');

    if (!source) {
        debugCounter++;
        if (debug) {
            console.log('source undefined ' + debugCounter);
        }
        if (debugCounter < threshold) {
            setTimeout(function () {
                configurePlayer();
            }, 100);
        }
    } else {
        interface.setVariable('videoUrl', '');

        var buttonFunction = interface.getVariable('buttonFunction');
        var buttonName;
        switch(buttonFunction){
            case 'play':
                buttonName = 'play_button';
                break;
            default:
                buttonName = 'next_button';
                break;
        }
        var showButton = setupButton(buttonName);

        var vid = document.getElementById("iframePlayer");
        vid.src = source;

        var myApp = new GlueFrame(document.getElementById("iframePlayer"), "Player");

        myApp.bind("player:video:ended", function (eventName, obj) {
            if (debug) {
                console.log("The event " + eventName + " fired and this object was returned: " + obj);
            }
            if (!showButton) {
                try {
                    if (typeof interface.getVariable('jumpDelay') !== 'undefined') {
                        var delay = Number(interface.getVariable('jumpDelay'));
                        if (debug) {
                            console.log("jumpDelay: " + delay);
                        }
                        setTimeout(function () {
                            navigate();
                            if (debug) {
                                console.log("Delayed jump");
                            }
                        }, delay);
                    } else {
                        navigate();
                    }
                } catch (e) {
                    if (debug) {
                        console.log(e);
                    }
                }
            } else {
                var buttonText = interface.getVariable('buttonText').replace(/ae/i, 'æ');
                interface.setVariable('buttonText', '');
                $('#' + buttonName).text(buttonText);
                setTimeout(function () {
                    $('#' + buttonName + '_container').show('fade', 750);
                }, 500);

            }

        });

        setupInternalNavigation(myApp);
    }
}

function nextButtonClick() {
    try {
        interface.jumpToSlide(interface.getVariable('jumpToSlideIndex'));
    } catch (e) {
        if (debug) {
            console.log(e);
        }
    }
}
function playButtonClick() {
    try {
        interface.playSlide();
    } catch (e) {
        if (debug) {
            console.log(e);
        }
    }
}

setup();