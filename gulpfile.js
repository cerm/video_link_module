/**
 * Created by Anders on 26/01/2017.
 */
var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    del = require('del'),
    htmlmin = require('gulp-htmlmin'),
    // htmlreplace = require('gulp-html-replace');
    gulpif = require('gulp-if'),
    useref = require('gulp-useref'),
    minifyCss = require('gulp-clean-css');



// gulp.task('minify-scripts', function() {
//     return gulp.src('./bower_components/api/*.js')
//         // .pipe(jshint('.jshintrc'))
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'))
//         // .pipe(concat('main.scripts'))
//         .pipe(gulp.dest('dist/scripts'))
//         .pipe(rename({ suffix: '.min' }))
//         .pipe(uglify())
//         .pipe(gulp.dest('dist/scripts'))
//         .pipe(notify({ message: 'Scripts task complete' }));
// });

// gulp.task('move-vendor-scripts', function() {
//     return gulp.src([
//         './bower_components/jquery/dist/*.min.scripts',
//         './bower_components/jqueryui/*.min.scripts',
//         './bower_components/lodash/dist/*.min.scripts'
//     ], {base: './bower_components/'})
//         .pipe(gulp.dest('dist/scripts/vendor'))
//         .pipe(notify({ message: 'move vendor scripts task complete' }));
// });

gulp.task('clean', function() {
    return del('dist/scripts');
});

// gulp.task('minify-html', function() {
//     return gulp.src('*.html')
//         .pipe(htmlreplace({scripts: 'scripts/vendor.scripts'}))
//         .pipe(htmlmin({collapseWhitespace: true}))
//         .pipe(gulp.dest('dist'));
// });

gulp.task('html', function () {
    return gulp.src('src/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        // .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});

gulp.task('build', ['clean'], function() {
    gulp.start('html');
});